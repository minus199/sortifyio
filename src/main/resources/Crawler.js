/**
 * Created by minus on 7/25/16.
 */
//http://fileinfo.com/filetypes/common
//assumed jQuery

function iterateTable(index, groupName) {
    var results = [];
    $("table").eq(index).find("tr").each(
        function (index, tr) {
            var tds = $(tr).find("td");

            var ext = $(tds[0]).text();
            var desc = $(tds[1]).text();

            var current = {
                "display_name": groupName,
                "category": groupName,
                "description": desc
            };

            if (ext.length > 0) results.push(current);
        }
    );
    return results;
}


function itertableAllTables(){
    var results = [];
    var tables = $("table");

    for (var i = 0; i < tables.length; i++){
        var groupName = tables.eq(i).find("th").eq(0).text();
        $.merge(results, iterateTable(i, groupName))
    }

    console.log(JSON.stringify(results));
}