package com.minus.sortifyio.controllers;

import com.minus.sortifyio.FileWalker;
import com.minus.sortifyio.ModelTreeFile;
import com.minus.sortifyio.enums.SortingTypes;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

import java.io.File;
import java.util.HashMap;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
class FileListView extends TreeView<ModelTreeFile> {
	final private FileWalker fileWalker;

	FileListView(FileWalker fileWalker) {
		this.fileWalker = fileWalker;

		setCellFactory(param -> new FileMenuCell());
		reload(SortingTypes.BY_TYPE);
	}

	void reload(SortingTypes sorting) {
		super.refresh();
		resetRoot();
		sorting.getSorter().sortInto(getRoot());
	}

	private void resetRoot() {
		setRoot(new TreeItem<>(new ModelTreeFile("all", "all", "all")));
		getRoot().setExpanded(true);
	}

	private void groupBy() {
		final HashMap<Object, Object> objectObjectHashMap = new HashMap<>();
		final TreeItem<ModelTreeFile> root = new TreeItem<>(new ModelTreeFile("root", "root", "root"));
		fileWalker.getAllFiles().stream().filter(ModelTreeFile::isFile).forEach(mf -> {
			final File file = mf.getFile();
			file.getAbsoluteFile().toPath().forEach(path -> {
				final TreeItem<ModelTreeFile> currentPathTreeItem = new TreeItem<>(new ModelTreeFile(path.toString(), "folder", "folder"));


				if (! root.getChildren().contains(currentPathTreeItem)) {
					//currentPathTreeItem.
				}

				System.out.println();
			});
		});
	}
}
