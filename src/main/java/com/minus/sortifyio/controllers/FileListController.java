package com.minus.sortifyio.controllers;

import com.minus.sortifyio.FileWalker;
import com.minus.sortifyio.ModelTreeFile;
import com.minus.sortifyio.Sortifyio;
import com.minus.sortifyio.enums.SortingTypes;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileListController extends BorderPane implements IController {

	private TextField pathInput = new TextField(Sortifyio.DEFAULT_PATH);
	private FileListView fileListView;
	private ChoiceBox<SortingTypes> sortingOptions = new ChoiceBox<>(FXCollections.observableArrayList(SortingTypes.values()));
	private VBox leftContainer = new VBox();

	public FileListController(FileWalker fileWalker) {
		fileListView = new FileListView(fileWalker);
		fileListView.prefHeightProperty().bind(prefHeightProperty());
		fileListView.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> setCenter(newValue.getValue().getFile() == null ? createMetaDataForType(newValue) : createMetaDataForFile(newValue)));

		leftContainer.getChildren().setAll(fileListView, sortingOptions);
		pathInput.setPromptText("Enter some path...");

		setTop(pathInput);
		setLeft(leftContainer);
		setStyle("-fx-background-color: white");

		sortingOptions.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> fileListView.reload(newValue));
		sortingOptions.getSelectionModel().select(SortingTypes.BY_TYPE);
	}

	private VBox createMetaDataForType(TreeItem<ModelTreeFile> newValue) {
		final VBox metaDataView = new VBox();
		metaDataView.setSpacing(10);

		buildMetaDataRow("Display name", newValue.getValue().getDisplayName(), metaDataView);
		buildMetaDataRow("Extension", newValue.getValue().getCategory(), metaDataView);

		return metaDataView;
	}

	private VBox createMetaDataForFile(TreeItem<ModelTreeFile> newValue) {
		final VBox metaDataView = new VBox();
		metaDataView.setSpacing(10);

		buildMetaDataRow("isDirectory", newValue.getValue().getFile().isDirectory(), metaDataView);
		buildMetaDataRow("getParent", newValue.getValue().getFile().getParent(), metaDataView);
		buildMetaDataRow("list", newValue.getValue().getFile().list(), metaDataView);
		buildMetaDataRow("toURI", newValue.getValue().getFile().toURI(), metaDataView);
		buildMetaDataRow("canExecute", newValue.getValue().getFile().canExecute(), metaDataView);
		buildMetaDataRow("canRead", newValue.getValue().getFile().canRead(), metaDataView);
		buildMetaDataRow("canWrite", newValue.getValue().getFile().canWrite(), metaDataView);
		buildMetaDataRow("getAbsolutePath", newValue.getValue().getFile().getAbsolutePath(), metaDataView);
		buildMetaDataRow("getFreeSpace", newValue.getValue().getFile().getFreeSpace(), metaDataView);
		buildMetaDataRow("getName", newValue.getValue().getFile().getName(), metaDataView);
		buildMetaDataRow("getTotalSpace", newValue.getValue().getFile().getTotalSpace(), metaDataView);
		buildMetaDataRow("getUsableSpace", newValue.getValue().getFile().getUsableSpace(), metaDataView);
		buildMetaDataRow("isAbsolute", newValue.getValue().getFile().isAbsolute(), metaDataView);
		buildMetaDataRow("isHidden", newValue.getValue().getFile().isHidden(), metaDataView);
		buildMetaDataRow("lastModified", newValue.getValue().getFile().lastModified(), metaDataView);

		return metaDataView;
	}

	private void buildMetaDataRow(String metaKey, Object metaValue, Pane metadataView) {
		String labelText = String.valueOf(metaValue);

		if (metaValue instanceof Object[]) {
			labelText = Stream.of(metaValue).map(obj -> String.valueOf(obj)).collect(Collectors.joining(", "));
		}

		if (metaValue instanceof Boolean) {
			labelText = (Boolean) metaValue ? "true" : "false";
		}

		final HBox hBox = new HBox(new Label(metaKey), new Label(labelText));
		hBox.setPadding(new Insets(5, 100, 5, 3));
		hBox.setSpacing(10);

		metadataView.getChildren().add(hBox);
	}
}
