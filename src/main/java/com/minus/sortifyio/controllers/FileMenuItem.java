package com.minus.sortifyio.controllers;

import com.minus.sortifyio.ModelTreeFile;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

import java.util.stream.Collectors;

/**
 * @deprecated
 * Created by minus on 7/25/16.
 */
public class FileMenuItem extends TreeItem<ModelTreeFile> {
	private final ObservableList<TreeItem<ModelTreeFile>> modifiedChildren;

	public FileMenuItem(ModelTreeFile value) {
		super(value);
		this.modifiedChildren = FXCollections.observableArrayList(
				value.getChildren().stream().map(FileMenuItem::new).collect(Collectors.toList()));
	}

	@Override
	public ObservableList<TreeItem<ModelTreeFile>> getChildren() {
		return modifiedChildren;
	}
}
