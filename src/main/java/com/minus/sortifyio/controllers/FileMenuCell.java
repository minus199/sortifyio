package com.minus.sortifyio.controllers;

import com.minus.sortifyio.ModelTreeFile;
import javafx.scene.control.TreeCell;

/**
 * Created by minus on 7/25/16.
 */
public class FileMenuCell extends TreeCell<ModelTreeFile>{

	@Override
	protected void updateItem(ModelTreeFile item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
			return;
		}

		setText(item.getDisplayName());
	}
}
