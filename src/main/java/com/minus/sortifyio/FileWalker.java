package com.minus.sortifyio;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by minus on 7/24/16.
 */
public class FileWalker extends SimpleFileVisitor<Path> {
	private final AllowedFileTypes allowedFileTypes = AllowedFileTypes.getInstance();
	private final List<ModelTreeFile> allFiles = new ArrayList();

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		return super.preVisitDirectory(dir, attrs);
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		final FileVisitResult fileVisitResult = super.visitFile(file, attrs);

		final File currentFile = file.toFile();
		if (currentFile.isDirectory())
			return fileVisitResult;


		appendFile(currentFile);

		return fileVisitResult;
	}

	private ModelTreeFile appendFile(File currentFile) {
		final String[] nameParts = currentFile.getName().split("\\.");
		String ext = nameParts.length < 2 ? null : nameParts[nameParts.length - 1];
		final ModelTreeFile modelTreeFile = new ModelTreeFile(currentFile.getName(), ext, "this is a file", currentFile);
		allFiles.add(modelTreeFile);
		return allowedFileTypes.getBySlug(ext).addChild(modelTreeFile);
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
		return super.visitFileFailed(file, exc);
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		return super.postVisitDirectory(dir, exc);
	}

	public List<ModelTreeFile> getAllFiles() {
		return allFiles;
	}
}
