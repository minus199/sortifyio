package com.minus.sortifyio.sorters;

import com.minus.sortifyio.ModelTreeFile;
import javafx.scene.control.TreeItem;

import java.util.List;

/**
 * Created by minus on 7/26/16.
 */
public interface TreeSorter {
	List<TreeItem<ModelTreeFile>> sort();
	void sortInto(TreeItem<ModelTreeFile> modelTreeFileTreeItem);

}
