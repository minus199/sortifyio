package com.minus.sortifyio.sorters;

import com.minus.sortifyio.AllowedFileTypes;
import com.minus.sortifyio.ModelTreeFile;
import javafx.scene.control.TreeItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minus on 7/26/16.
 */
public class ByType implements TreeSorter {

	@Override
	public void sortInto(TreeItem<ModelTreeFile> modelTreeFileTreeItem) {
		modelTreeFileTreeItem.getChildren().setAll(sort());
	}

	public List<TreeItem<ModelTreeFile>> sort() {
		final List<TreeItem<ModelTreeFile>> root = new ArrayList<>();

		AllowedFileTypes.getInstance().getMapped().stream()
				.filter(i -> i != null && i.getChildren().size() > 0)
				.forEach(mf -> {
					final TreeItem<ModelTreeFile> currentRoot = new TreeItem<>(mf); //a root for a specific extension
					mf.getChildren().forEach(sub_mf -> currentRoot.getChildren().add(new TreeItem<>(sub_mf)));
					root.add(currentRoot);
				});

		return root;
	}
}
