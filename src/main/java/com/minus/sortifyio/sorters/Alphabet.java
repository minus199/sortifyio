package com.minus.sortifyio.sorters;

import com.minus.sortifyio.ModelTreeFile;
import javafx.scene.control.TreeItem;

import java.util.List;

/**
 * Created by minus on 7/26/16.
 */
public class Alphabet implements TreeSorter {
	@Override
	public List<TreeItem<ModelTreeFile>> sort() {
		return null;
	}

	@Override
	public void sortInto(TreeItem<ModelTreeFile> modelTreeFileTreeItem) {
		modelTreeFileTreeItem.getChildren().setAll(sort());
	}
}
