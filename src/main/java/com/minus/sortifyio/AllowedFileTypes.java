package com.minus.sortifyio;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.minus.sortifyio.utils.FileUtils;

import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by minus on 7/25/16.
 */
public class AllowedFileTypes {
	private static final ModelTreeFile EXTENSION_UNKOWN = new ModelTreeFile("Unknown", "N/A", "All others");

	private final List<ModelTreeFile> extensions;

	private static final class InstanceContainer {
		private static final AllowedFileTypes instance = new AllowedFileTypes();
	}

	private AllowedFileTypes() {
		extensions = loadJson();
	}

	public static AllowedFileTypes getInstance() {
		return InstanceContainer.instance;
	}

	private List<ModelTreeFile> loadJson() {
		final String fileTypesSource = String.valueOf(AllowedFileTypes.class.getClassLoader().getResource("file_types.json"));
		try {
			final String rawTypes = FileUtils.readFile(fileTypesSource.replace("file:", ""), Charset.defaultCharset());
			return new Gson().fromJson(rawTypes, new TypeToken<List<ModelTreeFile>>() {
			}.getType());
		} catch (Exception ignored) {
			throw new RuntimeException(ignored);
		}
	}

	public final List<ModelTreeFile> getMapped() {
		return Collections.unmodifiableList(extensions);
	}

	public final ModelTreeFile getBySlug(final String ext) {
		if (ext == null) {
			return EXTENSION_UNKOWN;
		}

		final Optional<ModelTreeFile> first = extensions.stream()
				.filter(extObj ->Objects.equals(extObj.getCategory(), "." + ext.toUpperCase()))
				.findFirst();
		return first.isPresent() ? first.get() : EXTENSION_UNKOWN;
	}
}
