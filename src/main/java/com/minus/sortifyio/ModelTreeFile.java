package com.minus.sortifyio;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by minus on 7/25/16.
 */
public class ModelTreeFile {

	@SerializedName("display_name")
	private String displayName;
	private String category;
	private File file = null;
	private String description;

	private List<ModelTreeFile> children;

	public ModelTreeFile() {
		children = new ArrayList<>();
	}

	public ModelTreeFile(String displayName, String category, String description) {
		this(displayName, category, description, null);
	}

	public ModelTreeFile(String displayName, String category, String description, File file) {
		this.displayName = displayName;
		this.category = category;
		this.file = file;
		this.description = description;
		this.children = new ArrayList<>();
	}

	public ModelTreeFile addChild(ModelTreeFile modelTreeFile) {
		if (this.equals(modelTreeFile)) {
			throw new RuntimeException("Duplicate");
		}

		children.add(modelTreeFile);

		return this;
	}

	public List<ModelTreeFile> getChildren() {
		return children;
	}

	public String getDisplayName() {
		return displayName;
	}

	public boolean isFile(){
		return file != null && file.exists();
	}

	public File getFile() {
		return file;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (! (o instanceof ModelTreeFile)) return false;

		ModelTreeFile that = (ModelTreeFile) o;

		if (displayName != null ? ! displayName.equals(that.displayName) : that.displayName != null) return false;
		if (category != null ? ! category.equals(that.category) : that.category != null) return false;
		if (file != null ? ! file.equals(that.file) : that.file != null) return false;
		return description != null ? ! description.equals(that.description) : that.description != null;

	}

	@Override
	public int hashCode() {
		int result = displayName != null ? displayName.hashCode() : 0;
		result = 31 * result + (category != null ? category.hashCode() : 0);
		result = 31 * result + (file != null ? file.hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		return result;
	}

	public String getCategory() {
		return category;
	}
}
