package com.minus.sortifyio;

import com.minus.sortifyio.controllers.FileListController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.nio.file.Files;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class Sortifyio extends Application {
	//	public static final String DEFAULT_PATH = "/media/minus/684565A875F0516A/";
	public static final String DEFAULT_PATH = "/workspace/MiNuS/";

	@Override
	public void start(Stage primaryStage) {
		final FileWalker fileWalker = new FileWalker();
		try {
			Files.walkFileTree(new File(Sortifyio.DEFAULT_PATH).toPath(), fileWalker);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}


		FileListController fileListController = new FileListController(fileWalker);
		Scene scene = new Scene(fileListController, 800, 600);
		fileListController.prefHeightProperty().bind(scene.heightProperty());
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
