package com.minus.sortifyio.enums;

import com.minus.sortifyio.sorters.Alphabet;
import com.minus.sortifyio.sorters.ByType;
import com.minus.sortifyio.sorters.TreeSorter;

/**
 * Created by minus on 7/26/16.
 */
public enum SortingTypes {
	ALPHABET(1, "Alphabet", Alphabet.class),
	BY_TYPE(2, "By Type", ByType.class);

	private final Integer code;
	private final String displayName;
	private final Class<? extends TreeSorter> sorterKlazz;

	SortingTypes(Integer code, String displayName, Class<? extends TreeSorter> sorter) {
		this.code = code;
		this.displayName = displayName;
		sorterKlazz = sorter;
	}

	public Integer getCode() {
		return code;
	}

	public String getDisplayName() {
		return displayName;
	}

	public TreeSorter getSorter(){
		try {
			return sorterKlazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
}
